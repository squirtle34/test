import { store } from '../store/index';

const config = {
  scheme: 'com.guardiasapp.guardiasapp',
  schemeIOS: 'guardiasapp',
  searchTimeout: 60000, // ms
};

export default config;
