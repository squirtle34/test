import React, { Component } from 'react';
import { Text, View, Image,  Keyboard, StyleSheet, AppRegistry, TextInput, TouchableOpacity, ImageBackground, Platform, StatusBar, BackHandler, I18nManager } from 'react-native';
import { Container, Button, Icon, Right, Item, Input, Header, Footer, Left, Body, Title, Content, Form, Label} from 'native-base';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  login,
  setRefresing,
} from '../../store/User';

// Screen Styles
import styles from './styles';
import { Images } from '../../Themes/';

//import { StackNavigator } from 'react-navigation';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  doLogin: (username, user_id) => dispatch(login(username, user_id)),
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userEmail: '',
      userPassword: '',
      error: '',
    }
  }

  login = () => {
    const { userEmail, userPassword } = this.state;
    const { doLogin } = this.props;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (userEmail === "") {
      this.setState({ error: 'Ingrese un correo' });
    } else if (reg.test(userEmail) === false) {
      this.setState({ error: 'El Correo no es correcto' });
      return false;
    } else if (userPassword === "") {
      this.setState({ error: 'Ingrese la contraseña' });
    } else {
      alert("Bienvenido al sistema");
      doLogin(userEmail, "1234");
      this.props.navigation.navigate('App');
    }
    Keyboard.dismiss();
  }

    render() {
      StatusBar.setBarStyle('light-content', true);
      if(Platform.OS === 'android') {
        StatusBar.setBackgroundColor('transparent',true);
        StatusBar.setTranslucent(true);
      }
        return (
          <Container>
            <ImageBackground style={styles.ImageBackground} source={{uri : 'http://www.independentmediators.co.uk/wp-content/uploads/2016/02/placeholder-image.jpg'}}>
            </ImageBackground>
            <Content style={styles.content}>
              <Form style={styles.form}>
                <Item style={{marginBottom:5}}>
                  <Input keyboardType="email-address" placeholder='Email' textAlign={I18nManager.isRTL ? 'right' : 'left'} placeholderTextColor='#b7b7b7' style={styles.input1} onChangeText={userEmail => this.setState({userEmail})}/>
                </Item>
                <Item>
                  <Input placeholder='Password' textAlign={I18nManager.isRTL ? 'right' : 'left'} secureTextEntry={true} placeholderTextColor='#b7b7b7' style={styles.input1} onChangeText={userPassword => this.setState({userPassword})} />
                </Item>
                <Text style={{padding:10,color:'red'}}>{this.state.error}</Text>
              </Form>
              <Text style={{color:'#0071BC', textAlign:'center', fontWeight:'600'}}>Forgot your Password</Text>
              <TouchableOpacity info style={styles.signInBtn} onPress={this.login}>
                <Text autoCapitalize="words" style={styles.signInBtnText}>SIG IN</Text>
              </TouchableOpacity>

              <TouchableOpacity info style={styles.signInBtnfff} onPress={()=>{console.log('CreateAccount')}}>
                <Text autoCapitalize="words" style={styles.signInBtnTextblue}>CREATE ACCOUNT</Text>
              </TouchableOpacity>
            </Content>
          </Container>
        );
    }
}
