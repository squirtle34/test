import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { Platform, Animated, View } from 'react-native';
import { StackNavigator, SwitchNavigator, NavigationActions } from 'react-navigation';
import withBar from './components/WithBar';
import ButtonFloat from './components/ButtonFloat';
import ManagerBrokersInvest from '@utils/managerBrokersInvest';
import GradientHeader from './components/Header';
import ModalAddFunds from './components/ModalAddFunds';
import ModalPlaid from './components/ModalPlaid';

import Mixpanel from 'react-native-mixpanel';
import config from './constants/app';
import * as DeviceInfo from 'react-native-device-info';

import AuthLoadingScreen from './screens/AuthLoading';
import LoginScreen from './screens/Login';
import SignUpNavigation from './screens/SignUp';
import AppStack from './screens/Main';
import notifications from './utils/notifications';
import NavigationService from './utils/navigation';
import OnBoardingInitAppStack from './screens/OnBoardingInitApp';
import {
  setRefresing,
} from '@store/User'; // eslint-disable-line

const AuthStack = StackNavigator(
  {
    SignIn: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
      },
    },
    SignUp: SignUpNavigation,
  },
  {
    headerMode: 'screen',
    /*
    * Use modal on iOS because the card mode comes from the right,
    * which conflicts with the drawer example gesture
    */
    mode: 'card',
    navigationOptions: {
      header: props => (<GradientHeader {...props} />),
      headerStyle: {
        backgroundColor: 'transparent',
      },
    },
  },
);

const AppNavigator = SwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
    Init: OnBoardingInitAppStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch => ({
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

@withBar('#A00057')
@connect(mapStateToProps, mapDispatchToProps)
export default class Navigator extends Component {
  constructor(props) {
    super(props);
    this.state={
      visibleModalAddFunds: false,
      visibleModalPlaid: false,
    }
  }
  componentDidMount() {
    const { navigation, setRefresing } = this.props;
    notifications.setupComponents(NavigationService, setRefresing);
    notifications.on();
  }

  buttonFloat(){
    const { user, navigation } = this.props;
    console.log(this.state.asd);
    if (user.isAuth) {
      if(this.state.asd !== "Strategy_Details" &&
          this.state.asd !== "Referreals" && this.state.asd !== "Plaid" &&
            this.state.asd !== "AddFunds" && this.state.asd !== "Strategy" && this.state.asd !== "CommentsUpdate"
              && this.state.asd !== "ListInfo") {
        if (user.apex_account) {
          if ((user.apex_account.status === 'COMPLETE' || user.profile.ready_to_be_created || user.apex_account.status === 'PENDING')) {
            return (
              <ButtonFloat
                buttonColor="#160056"
                onPress={() => {
                  //NavigationService.navigate(this._goToAddFunds(), { over: true });
                  //this.setState({visibleModalSell : true });
                  this._goToAddFunds();
                }}
                />
            );
          }
        }
      }
    }
    return null;
  }

  _goToAddFunds = () => {
    const { user } = this.props;
    if (user.dashboard != null && user.dashboard.status != null && user.dashboard.status === 'PENDING') {
      if (this.props.user.profile.is_international === false) {
        this.setState({visibleModalPlaid: true});
      }
        this.setState({visibleModalPlaid: true});
    }
    if (user.profile != null && user.profile.is_international === true && (user.achs == null || user.achs.length === 0)) {
      this.setState({visibleModalAddFunds: true});
    }
      this.setState({visibleModalAddFunds: true});
  }

  onClose() {
    this.setState({
      visibleModalAddFunds: false,
      visibleModalPlaid: false,
    });
  }

  initiateMixpanel = async (currentState) => {
    try {
      const mpInstance = await Mixpanel.sharedInstanceWithToken(config.mixpanel_token);
      console.log(Mixpanel.getDistinctId((id) => {}));
      console.log('mpInstance:', mpInstance);
      const doMpStuff = await Promise.all([
        Mixpanel.identify(DeviceInfo.uniqueID),
        Mixpanel.set({ deviceName: DeviceInfo.deviceName }),
        Mixpanel.track(currentState+'_ViewScreen'),
      ]);
      console.log('doMpStuff:', doMpStuff);
    } catch (err) {
      console.log('Error initiating Mixpanel:', err);
    }
  };

  render() {
    const { resources, uriPrefix, user } = this.props;
    return (
      <View style={{flex:1}}>
      <AppNavigator
        uriPrefix={uriPrefix}
        ref={(navigatorRef) => {
         NavigationService.setTopLevelNavigator(navigatorRef);
       }}
      onNavigationStateChange={(prevState, currentState) => {
      let currentScreen = NavigationService.getCurrentRouteName(currentState);
      const prevScreen = NavigationService.getCurrentRouteName(prevState);
      if (currentScreen !== prevScreen) {
        if (currentScreen !== "Strategy_Details" && currentScreen !== "Strategy") {
          this.initiateMixpanel(currentScreen);
        }
      }
      this.setState({asd : currentScreen});
    }}
      />
    {this.buttonFloat()}
    <ModalAddFunds
      user={user}
      isVisible={this.state.visibleModalAddFunds}
      onClose={() => this.onClose()}
    />
    <ModalPlaid
      user={user}
      isVisible={this.state.visibleModalPlaid}
      onClose={() => this.onClose()}
    />
    </View>
    );
  }
}
