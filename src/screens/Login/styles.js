
import { Platform, StyleSheet, Dimensions } from 'react-native';
import { Fonts, Metrics, Colors } from '../../Themes/';

const styles = StyleSheet.create({
	ImageBackground: {
		width: Metrics.WIDTH,
		height: Metrics.HEIGHT * 0.50,
		zIndex: 111,
	},
	header: {
    backgroundColor: Colors.transparent,
    height: 50,
    borderBottomWidth: 0,
    ...Platform.select({
      ios: {},
      android: {
				marginTop: Fonts.moderateScale(25)
			}
    }),
		elevation: 0
  },
	left: {
		flex: 0.5,
		backgroundColor: 'transparent',
  },
	backArrow: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 30
  },
	body: {
		flex: 3,
		alignItems: 'center',
		backgroundColor: 'transparent'
  },
	textTitle: {
    color: Colors.snow,
    fontSize: Fonts.moderateScale(20),
    alignSelf: 'center',
	  fontFamily: Fonts.type.sfuiDisplaySemibold,
  },
	right: {
    flex: 0.5
  },
	content: {
		 backgroundColor: 'transparent',
		 height: Metrics.HEIGHT * 0.50,
	},
	profile: {
		borderWidth: 3,
		borderColor: Colors.snow,
		borderRadius: Metrics.WIDTH * 0.14,
		width: Metrics.WIDTH * 0.28,
		height: Metrics.WIDTH * 0.28,
	},
	form: {
		marginTop: 20,
		alignItems:'center',
		backgroundColor:'#fff',
    width: (Metrics.WIDTH * 0.92),
    borderRadius: 5,
    justifyContent: 'space-between',
    alignSelf: 'center',
    elevation: 1,
		bottom:15,
		borderWidth: 1,
		borderColor: "#fff",
		zIndex: 1111,
		shadowColor: '#000000',
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 3.0,
    },
	},
	input1: {
		fontFamily: Fonts.type.sfuiDisplayRegular
	},
	forgotlabel: {
		fontFamily: Fonts.type.sfuiDisplayRegular,
		fontSize: Fonts.moderateScale(12),
		justifyContent: 'center',
		alignSelf: 'flex-end',
		marginBottom: 15
	},
	btnSec: {
		width: Metrics.WIDTH,
		justifyContent:'center',
		backgroundColor:'#4cd964'
	},
	buttontext: {
		alignSelf:'center',
		fontFamily: Fonts.type.sfuiDisplaySemibold,
		color: Colors.snow,
		fontSize: Fonts.moderateScale(17)
	},
	containMainBg: {
    backgroundColor: 'transparent',
    height: (Metrics.HEIGHT * 0.50),
    width: (Metrics.WIDTH * 0.92),
    borderRadius: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 3,
		position: 'absolute',
		left: 15,
		right:15,
		bottom:15,
		borderColor: "transparent",
  },
	containPassword:{
		backgroundColor: 'transparent',
    height: (Metrics.HEIGHT * 0.07),
    width: (Metrics.WIDTH * 0.84),
    borderBottomLeftRadius: 5,
		borderBottomRightRadius: 5,
		borderBottomWidth: 1,
		borderLeftWidth: 1,
		borderRightWidth: 1,
		borderColor: "#ccc",
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
		elevation:1,
		position: 'absolute',
		left: 15,
		right:15,
		bottom:(Metrics.HEIGHT * 0.18),
	},
	signInBtn:{
		marginTop: 10,
		backgroundColor: "#0071BC",
		width: (Metrics.WIDTH * 0.84),
		borderRadius: 20,
		justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 3,
		height: 45,
	},
	signInBtnfff:{
		marginTop: 10,
		backgroundColor: "#fff",
		width: (Metrics.WIDTH * 0.84),
		borderRadius: 20,
		justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 3,
		height: 45,
		borderWidth:1,
		borderColor:'#0071bc'
	},
	signInBtnText:{
		marginTop: 2,
		color: "#fff",
		fontSize: Fonts.moderateScale(14),
		justifyContent: 'space-between',
		width: (Metrics.WIDTH  * 0.84),
		textAlign: 'center',
 		fontFamily: Fonts.type.helveticaNeueBold,
		letterSpacing: 1.85,
	},
	signInBtnTextblue:{
		marginTop: 2,
		color: "#0071BC",
		fontSize: Fonts.moderateScale(14),
		justifyContent: 'space-between',
		width: (Metrics.WIDTH  * 0.84),
		textAlign: 'center',
 		fontFamily: Fonts.type.helveticaNeueBold,
		letterSpacing: 1.85,
	},
	forgotPassword:{
		color: "#0691ce",
		fontSize: 10,
		height: (Metrics.HEIGHT * 0.05),
		width: (Metrics.WIDTH),
		alignItems: 'center',
		alignSelf: 'center',
		justifyContent: 'center',
		textAlign: 'center',
		backgroundColor: 'transparent',
		top:(Metrics.HEIGHT * 0.44),
 		fontFamily: Fonts.type.sfuiDisplayRegular,
	},




});
export default styles;
