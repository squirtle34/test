import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { Platform, Animated, View, Text } from 'react-native';
import { StackNavigator, SwitchNavigator, NavigationActions, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import NavigationService from './utils/navigation';
import AuthLoadingScreen from './screens/AuthLoading';
import LoginScreen from './screens/Login';
import AppStack from './screens/Main';
import {
  setRefresing,
} from '@store/User'; // eslint-disable-line

const AuthStack = createStackNavigator(
  {
    SignIn: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
      },
    },
  },
);

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch => ({
  setRefresing: refreshing => dispatch(setRefresing(refreshing)),
});

@connect(mapStateToProps, mapDispatchToProps)
export default class Navigator extends Component {
  constructor(props) {
    super(props);
    this.state={
    }
  }

  componentDidMount() {
    const { navigation, setRefresing } = this.props;
  }

  render() {
    const { resources, uriPrefix, user } = this.props;
    return (
      <AppNavigator
        uriPrefix={uriPrefix}
        ref={(navigatorRef) => {
         NavigationService.setTopLevelNavigator(navigatorRef);
        }}
        onNavigationStateChange={(prevState, currentState) => {
          let currentScreen = NavigationService.getCurrentRouteName(currentState);
          const prevScreen = NavigationService.getCurrentRouteName(prevState);
          if (currentScreen !== prevScreen) {
            console.log(currentScreen);
          }
        }}
      />
    );
  }
}
