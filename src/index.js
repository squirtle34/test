import React, { Component } from 'react';
import { Platform, SafeAreaView, View } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import axios from 'axios';
import { store, persistor } from './store';
import Navigator from './Navigator';
import config from './constants/app';

export default class App extends Component<{}> {
  componentDidMount() {
    try {
      console.log('open');
    } catch (e) {
      console.log(e);
    }
  }

  componentWillUnmount() {
    // stop listening for events
  }

  render() {
    const prefix = Platform.OS === 'android'
      ? `${config.scheme}://${config.scheme}/` : `${config.scheme}://`;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <Navigator uriPrefix={prefix} />
          </PersistGate>
        </Provider>
      </SafeAreaView>
    );
  }
}
