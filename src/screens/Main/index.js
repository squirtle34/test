import React, { Component } from 'react';
import { Image, Platform, Dimensions, Text, View } from 'react-native';
import { StackNavigator, createSwitchNavigator } from 'react-navigation';

import NavigationService from '@utils/navigation';

import HomeScreen from './screens/Home';

const AppStack = createSwitchNavigator({
  Home: { screen: HomeScreen },

 });

export default AppStack;
