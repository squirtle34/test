import React, { Component } from 'react';
import { AppRegistry,View,Text,StyleSheet
,Button,TouchableOpacity,StatusBar,Image } from 'react-native';
import { connect } from 'react-redux';
import {
  logout,
} from '@store/User';

const mapStateToProps = state => ({
  user: state.user,
});
const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(logout())
});
@connect(mapStateToProps, mapDispatchToProps)
export default class Home extends Component{

	constructor(props) {
    super(props);
    this.state = {

    };
		console.log(this.props.user);
  }

	componentDidMount() {

  }

  componentWillUnmount() {

  }

	render(){
		const { navigate } = this.props.navigation;
		return(
      <View style={styles.container}>
    		<TouchableOpacity
      		onPress={()=> {
						this.props.doLogout();
						this.props.navigation.navigate({
                routeName: 'Auth',
                params: {},
                action: {},
                key: 'Auth',
              });
					}}
      		style={styles.btn2}>
    		    <Text style={styles.btnText}>LOGOUT</Text>
    		</TouchableOpacity>
    </View>
		);
	}
}
const styles = StyleSheet.create({
	container:{
    flex:1,
    alignSelf: 'center',
    alignItems:'center',
		justifyContent:'center',
	},
	btn2:{
    alignItems:'center',
		backgroundColor:'#0691ce',
		padding:10,margin:10,width:'95%'
	},
	btnText:{
		color:'#fff',fontWeight:'bold'
	},
});


AppRegistry.registerComponent('home', () => home);
