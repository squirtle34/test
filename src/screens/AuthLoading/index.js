import React, { Component } from 'react';
import { View, Text, StatusBar, ActivityIndicator, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import config from '@constants/app';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => ({
  user: state.user,

});
@connect(mapStateToProps)
export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myKey: '',
    };
  this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
   _bootstrapAsync = () => {
    const isAuth = this.props.user.isAuth;
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(isAuth ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
